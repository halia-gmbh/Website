import React from 'react'
import { Link } from 'react-router-dom';
import * as ROUTES from '../../constants/routes';
//SCSS



function Footer() {
   
  
    return (
        <div >
            <footer class="page-footer  bg-dark">
                <div class="container-fluid text-center">
                    <hr class="my-3" />
                    <div class="row">
                        <div class="col-sm-3 ">
                            <Link className="FooterNav" to={ROUTES.LANDING}>
                                <h6>HOME</h6>
                            </Link>
                        </div>
                        <div class="col-sm-3">
                            <Link className="FooterNav" to={ROUTES.LANDING}>
                                <h6>KONTAKT</h6>
                            </Link>
                        </div>
                        <div class="col-sm-3">
                            <Link className="FooterNav" to={ROUTES.IMPRESSUM}>
                                <h6>IMPRESSUM</h6>
                            </Link>
                        </div>
                        <div class="col-sm-3">
                            <Link className="FooterNav" to={ROUTES.DATENSCHUTZ}>
                                <h6>DATENSCHUTZ</h6>
                            </Link>
                        </div>
                    </div>
                   
                    <hr class="my-4" />
                    <div className="borderline"></div>
                    <div class="footer-copyright text-center py-3"> &copy; {new Date().getFullYear()} Copyright:{" "}
                        <a href="https://gruppe2swe-a7f9b.web.app/">Halia's Dungeon</a>
                    </div>
                </div>
            </footer>
           
        </div>

    )
}

export default Footer
