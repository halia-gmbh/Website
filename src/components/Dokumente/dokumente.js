import React, { Component }  from 'react';
import moment from 'moment';
import moment2 from 'moment/locale/de';
import { withAuthorization } from '../Session';

class Dokumente extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      dokumente: [],
    };
  }

  componentDidMount() {
    this.setState({ loading: true });
 
    this.unsubscribe = this.props.firebase
      .dokumente()
      .orderBy('abgabe', 'desc')
      .onSnapshot(snapshot => {
        let dokumente = [];
 
        snapshot.forEach(doc =>
          dokumente.push({ ...doc.data(), did: doc.id }),
        );
 
        this.setState({
          dokumente,
          loading: false,
        });
      });
  }

  componentWillUnmount() {
    this.unsubscribe();
  }

  render() {
    const { dokumente, loading } = this.state;

    return (
      <div class="container">
        <hr class="my-5" />
                <h2 class="mb-5 font-weight-bold text-center">Private Dokumente</h2>

        {loading && <div>Loading ...</div>}

        <DokumenteList dokumente={dokumente} />
      </div>
    );
  }
}

const DokumenteList = ({ dokumente }) => (
  <>
  
    {dokumente.map(dokument => (
      <div >
      <div class="card mb-3">
          {/* <img class="card-img-top" alt="Bild"/> */}
          <div  class="card-body">
              <h5 class="card-title">{dokument.titel}</h5>
              <p class="card-text">
                  {dokument.inhalt}
              </p>
              <a class="btn btn-primary" href={dokument.pdf}>Zur PDF</a>                 
          </div>
          <div class="card-footer"> {  moment(dokument.abgabe.toDate()).format("LL")}</div>
      </div>
  </div>
    ))}
  </>
);
 
const condition = authUser => !!authUser;
 
export default withAuthorization(condition)(Dokumente);