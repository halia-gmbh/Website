import React from 'react';
import { Link } from 'react-router-dom';
 
import SignOutButton from '../SignOut/signOut';
import * as ROUTES from '../../constants/routes';
import { AuthUserContext } from '../Session';

//import './Navbar.scss'

const Navigation = ({ authUser }) => (
  <div> 
    <AuthUserContext.Consumer>
      {
        authUser =>
        authUser ? <NavigationAuth /> : <NavigationNonAuth />
      }
    </AuthUserContext.Consumer>
  </div>
);


const NavigationAuth = () => (
  <nav className="navbar sticky-top navbar-expand-lg navbar-dark bg-dark">
            <Link className="navbar-brand" to="/"> 
            {/* <img
        src={Logo}
        width="33"
        height="33"
        className="d-inline-block align-top"
        alt="React Bootstrap logo"
      />  */}
      Gruppe 2 SWE</Link>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarResponsive">
              <ul className="navbar-nav ml-auto">
              <Link className="nav-link" to={ROUTES.LANDING}>Home<span className="sr-only">(current)</span></Link>
              <Link className="nav-link" to={ROUTES.ALLGEMEIN}>Allgemeine Dokumente <span className="sr-only">(current)</span></Link>
              <Link className="nav-link" to={ROUTES.RECHERCHE}>Recherche <span className="sr-only">(current)</span></Link>
              <Link className="nav-link" to={ROUTES.ENTWURF}>Entwurf <span className="sr-only">(current)</span></Link>
              <Link className="nav-link" to={ROUTES.DOKUMENTE}>Private Dokumente <span className="sr-only">(current)</span></Link>
              <Link className="nav-link" to={ROUTES.ACCOUNT}>Account <span className="sr-only">(current)</span></Link>
              <Link className="nav-link" to={ROUTES.IMPRESSUM}>Impressum <span className="sr-only">(current)</span></Link>
              <Link className="nav-link" to={ROUTES.DATENSCHUTZ}>Datenschutz <span className="sr-only">(current)</span></Link>
             
              <SignOutButton />

              </ul>
              
            </div>
        </nav>
);

const NavigationNonAuth = () => (
  <nav className="navbar sticky-top navbar-expand-lg navbar-dark bg-dark">
            <Link className="navbar-brand" to="/"> 
            {/* <img
        src={Logo}
        width="33"
        height="33"
        className="d-inline-block align-top"
        alt="React Bootstrap logo"
      />  */}
      Halia's Dungeon</Link>
            <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarResponsive">
              <ul className="navbar-nav ml-auto">
              <Link className="nav-link" to={ROUTES.LANDING}>Home<span className="sr-only">(current)</span></Link>
              <Link className="nav-link" to={ROUTES.ALLGEMEIN}>Allgemeine Dokumente <span className="sr-only">(current)</span></Link>
              <Link className="nav-link" to={ROUTES.RECHERCHE}>Recherche <span className="sr-only">(current)</span></Link>
              <Link className="nav-link" to={ROUTES.ENTWURF}>Definition und Entwurf <span className="sr-only">(current)</span></Link>
              <Link className="nav-link" to={ROUTES.IMPRESSUM}>Impressum <span className="sr-only">(current)</span></Link>
              <Link className="nav-link" to={ROUTES.DATENSCHUTZ}>Datenschutz <span className="sr-only">(current)</span></Link>
              <Link className="nav-link" to={ROUTES.SIGN_IN}>Sign In <span className="sr-only">(current)</span></Link>
              </ul>
              
            </div>
        </nav>
);

 
export default Navigation;