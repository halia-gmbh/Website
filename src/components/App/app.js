import React from 'react';
import '../../mdb/css/bootstrap.css';
import '../../mdb/css/mdb.css';

import { BrowserRouter as Router, Route } from 'react-router-dom';


 
import Navigation from '../Navigation/navigation';
import Footer from '../Footer/footer';
import LandingPage from '../Landing/landing';
import AllgemeineDokumente from '../Dokumente_Allgemein/dokumente_Allgemein';
import EntwurfDokumente from '../Dokumente_Entwurf/dokumente_Enwurf';
import RechercheDokumente from '../Dokumente_Recherche/dokumente_Recherche';

import SignInPage from '../SignIn/signIn';
import PasswordForgetPage from '../PasswordForget/passwordForget';
import PrivateDokumente from '../Dokumente/dokumente';
import AccountPage from '../Account/account';
import AdminPage from '../Admin/admin';
import Impressum from '../Impressum/impressum';
import Datenschutz from '../Datenschutz/datenschutz';

import Cookies from '../Cookies/cookies'

import * as ROUTES from '../../constants/routes'; 
import { withAuthentication } from '../Session';
const App = () => (
  <Router>
    <div>
      <Navigation />
      <Cookies/>
     
      <Route exact path={ROUTES.LANDING} component={LandingPage} />      
      <Route path={ROUTES.SIGN_IN} component={SignInPage} />
      <Route path={ROUTES.ENTWURF} component={EntwurfDokumente} />
      <Route path={ROUTES.ALLGEMEIN} component={AllgemeineDokumente} />
      <Route path={ROUTES.RECHERCHE} component={RechercheDokumente} />
      <Route path={ROUTES.PASSWORD_FORGET} component={PasswordForgetPage} />
      <Route path={ROUTES.DOKUMENTE} component={PrivateDokumente} />
      <Route path={ROUTES.ACCOUNT} component={AccountPage} />
      <Route path={ROUTES.ADMIN} component={AdminPage} />
      <Route path={ROUTES.IMPRESSUM} component={Impressum} />
      <Route path={ROUTES.DATENSCHUTZ} component={Datenschutz} />
      
      <Footer />
    </div>
  </Router>
);
 
export default withAuthentication(App);