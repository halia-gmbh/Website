import React, { Component } from 'react';

import { Link, withRouter } from 'react-router-dom';

import { withFirebase } from '../Firebase';
import { Form, Button } from 'react-bootstrap';
import FileUploader from "react-firebase-file-uploader";
import firebase from "firebase";
import * as ROUTES from '../../constants/routes';

const CreateDokument = () => (
    <div>
        <CreateDokumentForm/>
    </div>
)
const INITIAL_STATE = {
    titel: '',
    inhalt: '',
    pdf: '',
    abgabe:  new Date(), 
    error: null,
  };
class CreateDokumentFormBase extends Component {
    constructor(props) {
        super(props);
    
        this.state = { ...INITIAL_STATE };
      }
   
   onSubmit = (event) => {
        const { titel, inhalt,pdf,abgabe } = this.state;
        
            // Create a user in your Firebase realtime database
           
        this.props.firebase
          .dokument('test')
          .set({
            titel,
            inhalt,
            pdf,
            abgabe
          },
          {merge: true},
          )
          .then(() => {
            this.setState({ ...INITIAL_STATE });
            this.props.history.push(ROUTES.ADMIN);
          })
          .catch(error => {
            this.setState({ error });
          });
        
          event.preventDefault();

    
}
    onChange = event => {
        this.setState({ [event.target.name]: event.target.value });
      };
    onPDFChange = (e) => {
        this.setState({
            pdf: e.target.value
        })    
    }
    onDateChange = (e) => {
        this.setState({
            datum: new Date(e.target.value)
        })
    }

    onUploadSuccess = filename => {
        this.setState({ Filename: filename, progress: 100, isUploading: false });
        firebase
            .storage()
            .ref('dokument_pdf')
            .child(filename)
            .getDownloadURL()
            .then(url => this.setState({ pdf: url }));
    };


   

    // https://www.youtube.com/watch?v=SvTfX7t_qSc&t=42s
    render() {        
        return (
            <React.Fragment>
                <Form className="container" onSubmit={this.onSubmit}>
                    <Form.Group controlID="formBasicText">
                        <Form.Label >Titel</Form.Label>
                        <Form.Control type="text" id="titel" onChange={this.onChange} placeholder="Titel des Artikels" required="required" />
                    </Form.Group>
                    <Form.Group controlID="formBasicText">
                        <Form.Label>Inhalt</Form.Label>
                        <Form.Control type="text" as="textarea" rows="5" id="inhalt" onChange={this.onChange} placeholder="Inhalt des Artikels" required="required" />
                    </Form.Group>
                    <Form.Group controlID="formBasicText">
                        <label class="btn-primary"style={{ padding: 10, borderRadius: 4, cursor: 'pointer' }}>
                            PDF hinzufügen
                            <FileUploader 
                                hidden
                                accept="pdf"
                                storageRef={firebase.storage().ref('dokument_pdf')}
                                onUploadStart={this.handleUploadStart}
                                onUploadError={this.handleUploadError}
                                onUploadSuccess={this.handleUploadSuccess}
                                onProgress={this.handleProgress}
                            />
                            
                        </label>
                        {this.state.Filename}

                    </Form.Group>
                    <Form.Group controlID="formBasicText">
                        <Form.Label>Abgabedatum</Form.Label>
                        <input class="form-control" type="datetime-local" id="abgabe" onChange={this.onDateChange}  required="required" /> 
                    </Form.Group>  
                    <Button variant="primary" type="submit">
                        Erstellen
                    </Button>
                </Form>
            </React.Fragment>
        )
    }
}

const CreateDokumentForm = withRouter(withFirebase(CreateDokumentFormBase));

export default CreateDokument;



