import React, { Component } from 'react';
import CreateDokument from './createDokument';
import { withFirebase } from '../Firebase';
import { withAuthorization } from '../Session';

class AdminPage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      users: [],
    };
  }

  componentDidMount() {
    this.setState({ loading: true });
 
    this.unsubscribe = this.props.firebase
      .users()
      .onSnapshot(snapshot => {
        let users = [];
 
        snapshot.forEach(doc =>
          users.push({ ...doc.data(), uid: doc.id }),
        );
 
        this.setState({
          users,
          loading: false,
        });
      });
  }

  componentWillUnmount() {
    this.unsubscribe();
  }

  render() {
    const { users, loading } = this.state;

    return (
      <div class="container">
        <hr class="my-5" />
        <h2 class="mb-5 font-weight-bold text-center">Admin</h2>
        <CreateDokument/>
        
      </div>
    );
  }
}



const condition = authUser => !!authUser;
 
export default withAuthorization(condition)(AdminPage);