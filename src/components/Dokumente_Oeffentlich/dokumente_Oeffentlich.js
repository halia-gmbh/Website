import React, { Component }  from 'react';
import moment from 'moment';
import moment2 from 'moment/locale/de';
import { withAuthorization } from '../Session';

class Dokumente_Oeffentlich extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      dokumente_oeffentlich: [],
    };
  }

  componentDidMount() {
    this.setState({ loading: true });
 
    this.unsubscribe = this.props.firebase
      .dokumente_oeffentlich()
      .orderBy('abgabe', 'desc')
      .onSnapshot(snapshot => {
        let dokumente_oeffentlich = [];
 
        snapshot.forEach(doc =>
          dokumente_oeffentlich.push({ ...doc.data(), doid: doc.id }),
        );
 
        this.setState({
          dokumente_oeffentlich,
          loading: false,
        });
      });
  }

  componentWillUnmount() {
    this.unsubscribe();
  }

  render() {
    const {dokumente_oeffentlich, loading } = this.state;

    return (
      <div class="container">
        <hr class="my-5" />
                <h2 class="mb-5 font-weight-bold text-center">Öffentliche Dokumente</h2>

        {loading && <div>Loading ...</div>}

        <DokumenteList_Oeffentlich dokumente_oeffentlich={dokumente_oeffentlich} />
      </div>
    );
  }
}

const DokumenteList_Oeffentlich = ({ dokumente_oeffentlich }) => (
  <>
  
    {dokumente_oeffentlich.map(dokument_oeffentlich => (
      <div >
      <div class="card mb-3">
          {/* <img class="card-img-top" alt="Bild"/> */}
          <div  class="card-body">
              <h5 class="card-title">{dokument_oeffentlich.titel}</h5>
              <p class="card-text">
                  {dokument_oeffentlich.inhalt}
              </p>
              <a class="btn btn-primary" href={dokument_oeffentlich.pdf}>Zur PDF</a>                 
          </div>
          <div class="card-footer"> {  moment(dokument_oeffentlich.abgabe.toDate()).format("LL")}</div>
      </div>
  </div>
    ))}
  </>
);
const condition = authUser => true; 

export default withAuthorization(condition)(Dokumente_Oeffentlich);
