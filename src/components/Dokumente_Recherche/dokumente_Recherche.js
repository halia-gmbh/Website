import React, { Component }  from 'react';
import moment from 'moment';
import moment2 from 'moment/locale/de';
import { withAuthorization } from '../Session';

class Dokumente_Recherche extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      dokumente_recherche: [],
    };
  }

  componentDidMount() {
    this.setState({ loading: true });
 
    this.unsubscribe = this.props.firebase
      .dokumente_recherche()
      .orderBy('abgabe', 'desc')
      .onSnapshot(snapshot => {
        let dokumente_recherche = [];
 
        snapshot.forEach(doc =>
          dokumente_recherche.push({ ...doc.data(), doid: doc.id }),
        );
 
        this.setState({
          dokumente_recherche,
          loading: false,
        });
      });
  }

  componentWillUnmount() {
    this.unsubscribe();
  }

  render() {
    const {dokumente_recherche, loading } = this.state;

    return (
      <div class="container">
        <hr class="my-5" />
                <h2 class="mb-5 font-weight-bold text-center">Recherche Dokumente</h2>

        {loading && <div>Loading ...</div>}

        <DokumenteList_Recherche dokumente_recherche={dokumente_recherche} />
      </div>
    );
  }
}

const DokumenteList_Recherche = ({ dokumente_recherche }) => (
  <>
  
    {dokumente_recherche.map(dokument_recherche => (
      <div >
      <div class="card mb-3">
          {/* <img class="card-img-top" alt="Bild"/> */}
          <div  class="card-body">
              <h5 class="card-title">{dokument_recherche.titel}</h5>
              <p class="card-text">
                  {dokument_recherche.inhalt}
              </p>
              <a class="btn btn-primary" href={dokument_recherche.pdf}>Zum Dokument</a>                 
          </div>
          <div class="card-footer"> {  moment(dokument_recherche.abgabe.toDate()).format("LL")}</div>
      </div>
  </div>
    ))}
  </>
);
const condition = authUser => true; 

export default withAuthorization(condition)(Dokumente_Recherche);
