import React from 'react';
import './landing.css'
import { Link } from 'react-router-dom';
import * as ROUTES from '../../constants/routes';

const Landing = () => (
        <>
                <header>
                        <div id="intro">
                                <div class="mask ">
                                        <div class="container-fluid d-flex align-items-center justify-content-center h-100">
                                                <div class="row d-flex justify-content-center text-center">
                                                        <div class="col-md-12">
                                                                <h1 class="display-4 font-weight-bold white-text pt-5 mb-2" style={{ marginTop: "20%" }}>Halia's Dungeon</h1>
                                                                <hr class="hr-light" />
                                                                <h3 class="white-text my-4">Unser Produkt <br /> jetzt testen!</h3>
                                                                <a href="http://193.196.55.121/"><button type="button" class="btn btn-red" style={{ fontSize: 18 }}>Halia's Dungeon<i class="fa fa-book ml-2"></i></button></a>
                                                                <a href="http://81.169.139.3/">
                                                                        <button type="button" className="btn btn-red"
                                                                                style={{fontSize: 18}}>Dokumentation<i
                                                                            className="fa fa-book ml-2"></i></button>
                                                                </a>
                                                        </div>
                                                </div>
                                        </div>
                                </div>
                        </div>
                </header>
                <div class="container">
                        <hr class="my-5" />
                        <h2 class="mb-5 font-weight-bold text-center">Über uns</h2>

                        <div class="row row-cols-1 row-cols-md-3 g-4">
                                <div class="col">
                                        <div class="card h-100">
                                                <div class="card-body">
                                                        <h5 class="card-title text-center">Dominik Sauerer</h5>
                                                        <p class="card-text">
                                                                Projektleiter
                                                        </p>
                                                </div>
                                                <div class="card-footer">
                                                        <small class="text-muted">14.03.2021</small>
                                                </div>
                                        </div>
                                </div>
                                <div class="col">
                                        <div class="card h-100">
                                                <div class="card-body">
                                                        <h5 class="card-title text-center">Aaron Stefan Vermeulen</h5>
                                                        <p class="card-text">
                                                                Technischer Assistent und Verantwortlicher für Implementierung
                                                        </p>
                                                </div>
                                                <div class="card-footer">
                                                        <small class="text-muted">14.03.2021</small>
                                                </div>
                                        </div>
                                </div>
                                <div class="col">
                                        <div class="card h-100">
                                                <div class="card-body">
                                                        <h5 class="card-title text-center">Marcel Burbach</h5>
                                                        <p class="card-text">
                                                                Verantwortlicher für Qualitätssicherung und Dokumentation
                                                                 </p>
                                                </div>
                                                <div class="card-footer">
                                                        <small class="text-muted">14.03.2021</small>
                                                </div>
                                        </div>
                                </div>
                        </div>
                        <div class="row row-cols-1 row-cols-md-3 g-4 my-4">
                                <div class="col ">
                                        <div class="card h-100">
                                                <div class="card-body">
                                                        <h5 class="card-title text-center">Anton Rösiger</h5>
                                                        <p class="card-text">
                                                                Verantwortlicher für Modellierung
                                                                 </p>
                                                </div>
                                                <div class="card-footer">
                                                        <small class="text-muted">14.03.2021</small>
                                                </div>
                                        </div>
                                </div>
                                <div class="col">
                                        <div class="card h-100">
                                                <div class="card-body">
                                                        <h5 class="card-title text-center">Jan Kupferschmid</h5>
                                                        <p class="card-text">
                                                                Verantwortlicher für Tests und Testautomatisierung
                                                                  </p>
                                                </div>
                                                <div class="card-footer">
                                                        <small class="text-muted">14.03.2021</small>
                                                </div>
                                        </div>
                                </div>
                                <div class="col">
                                        <div class="card h-100">
                                                <div class="card-body">
                                                        <h5 class="card-title text-center">Ruben Herbstreuth</h5>
                                                        <p class="card-text">
                                                                Verantwortlicher für Recherche
                                                                  </p>
                                                </div>
                                                <div class="card-footer">
                                                        <small class="text-muted">14.03.2021</small>
                                                </div>
                                        </div>
                                </div>

                        </div>
                        <hr class="my-5" />
                        <h2 class="mb-5 font-weight-bold text-center">Über das Projekt</h2>
                        <p class="text">
                                Halia’s Dungeon - benannt nach der Göttin Halia, die sich aus Frust in das weite Meer stürzte,
                                entsteht ein Multi-User Dungeon Server, mit dem Benutzer sich ihre eigenen Abenteuer erschaffen
                                können. Das fertige Produkt umfasst einen Editor zum Erstellen eigener Dungeons, eine Host-Funktion mit dem man Spiele eröffnen kann und natürlich der Möglichkeit, andere Spieler beitreten
                                zu lassen, die Raum für Raum erkunden können und in Interaktion mit dem Spielleiter viel Spaß
                                haben können. <br /><br />
                                Das Produkt entsteht im Rahmen der Dual Studierenden über 10 Wochen in Zusammenarbeit mit
                                ihren Betreuern.
                                <br /><br />
                                Zusammen sorgen wir dafür, dass Träume wahr werden! Haben Sie Fragen? Dann melden Sie sich
                                gerne bei unserem Projektleiter:<br /> Dominik Sauerer –<a href="mailto:i19042@hb.dhbw-stuttgart.de"> i19042@hb.dhbw-stuttgart.de</a>


                        </p>
                        <hr class="my-5" />


                </div>
        </>
);

export default Landing;