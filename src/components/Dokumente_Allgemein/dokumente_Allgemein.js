import React, { Component }  from 'react';
import moment from 'moment';
import moment2 from 'moment/locale/de';
import { withAuthorization } from '../Session';

class Dokumente_Allgemein extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      dokumente_allgemein: [],
    };
  }

  componentDidMount() {
    this.setState({ loading: true });
 
    this.unsubscribe = this.props.firebase
      .dokumente_allgemein()
      .orderBy('abgabe', 'desc')
      .onSnapshot(snapshot => {
        let dokumente_allgemein = [];
 
        snapshot.forEach(doc =>
          dokumente_allgemein.push({ ...doc.data(), doid: doc.id }),
        );
 
        this.setState({
          dokumente_allgemein,
          loading: false,
        });
      });
  }

  componentWillUnmount() {
    this.unsubscribe();
  }

  render() {
    const {dokumente_allgemein, loading } = this.state;

    return (
      <div class="container">
        <hr class="my-5" />
                <h2 class="mb-5 font-weight-bold text-center">Allgemeine Dokumente</h2>

        {loading && <div>Loading ...</div>}

        <DokumenteList_Allgemein dokumente_allgemein={dokumente_allgemein} />
      </div>
    );
  }
}

const DokumenteList_Allgemein = ({ dokumente_allgemein }) => (
  <>
  
    {dokumente_allgemein.map(dokument_allgemein => (
      <div >
      <div class="card mb-3">
          {/* <img class="card-img-top" alt="Bild"/> */}
          <div  class="card-body">
              <h5 class="card-title">{dokument_allgemein.titel}</h5>
              <p class="card-text">
                  {dokument_allgemein.inhalt}
              </p>
              <a class="btn btn-primary" href={dokument_allgemein.pdf}>Zum Dokument</a>                 
          </div>
          <div class="card-footer"> {  moment(dokument_allgemein.abgabe.toDate()).format("LL")}</div>
      </div>
  </div>
    ))}
  </>
);
const condition = authUser => true; 

export default withAuthorization(condition)(Dokumente_Allgemein);
