import React from 'react';
 
import { PasswordForgetForm } from '../PasswordForget/passwordForget';
import PasswordChangeForm from '../PasswordChange/passwordChange';
import { AuthUserContext, withAuthorization } from '../Session';

const AccountPage = () => (
  <AuthUserContext.Consumer>
    {authUser => (
      <div class="container">
        <hr class="my-5" />
                <h2 class="mb-5 font-weight-bold text-center">Account: {authUser.email}</h2>
                <h5 >Password Forget:</h5>
        <PasswordForgetForm />
        <h5 >Password Set:</h5>
        <PasswordChangeForm />
      </div>
    )}
  </AuthUserContext.Consumer>
);
 
const condition = authUser => !!authUser;
 
export default withAuthorization(condition)(AccountPage);