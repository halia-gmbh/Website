import React, { Component }  from 'react';
import moment from 'moment';
import moment2 from 'moment/locale/de';
import { withAuthorization } from '../Session';

class Dokumente_Entwurf extends Component {
  constructor(props) {
    super(props);

    this.state = {
      loading: false,
      dokumente_entwurf: [],
    };
  }

  componentDidMount() {
    this.setState({ loading: true });
 
    this.unsubscribe = this.props.firebase
      .dokumente_entwurf()
      .orderBy('abgabe', 'desc')
      .onSnapshot(snapshot => {
        let dokumente_entwurf = [];
 
        snapshot.forEach(doc =>
          dokumente_entwurf.push({ ...doc.data(), doid: doc.id }),
        );
 
        this.setState({
          dokumente_entwurf,
          loading: false,
        });
      });
  }

  componentWillUnmount() {
    this.unsubscribe();
  }

  render() {
    const {dokumente_entwurf, loading } = this.state;

    return (
      <div class="container">
        <hr class="my-5" />
                <h2 class="mb-5 font-weight-bold text-center">Dokumente zu Definition und Entwurf</h2>

        {loading && <div>Loading ...</div>}

        <DokumenteList_Entwurf dokumente_entwurf={dokumente_entwurf} />
      </div>
    );
  }
}

const DokumenteList_Entwurf = ({ dokumente_entwurf }) => (
  <>
  
    {dokumente_entwurf.map(dokument_entwurf => (
      <div >
      <div class="card mb-3">
          {/* <img class="card-img-top" alt="Bild"/> */}
          <div  class="card-body">
              <h5 class="card-title">{dokument_entwurf.titel}</h5>
              <p class="card-text">
                  {dokument_entwurf.inhalt}
              </p>
              <a class="btn btn-primary" href={dokument_entwurf.pdf}>Zum Dokument</a>                 
          </div>
          <div class="card-footer"> {  moment(dokument_entwurf.abgabe.toDate()).format("LL")}</div>
      </div>
  </div>
    ))}
  </>
);
const condition = authUser => true; 

export default withAuthorization(condition)(Dokumente_Entwurf);
