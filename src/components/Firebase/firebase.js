import app from 'firebase/app';
import 'firebase/auth'
import 'firebase/firestore';
const config = {
  apiKey: "AIzaSyAHoinhbttRzcExY34gileW6zenHr6_Wq4",
  authDomain: "gruppe2swe-a7f9b.firebaseapp.com",
  databaseURL: "https://gruppe2swe-a7f9b-default-rtdb.europe-west1.firebasedatabase.app",
  projectId: "gruppe2swe-a7f9b",
  storageBucket: "gruppe2swe-a7f9b.appspot.com",
  messagingSenderId: "44311916194",
  appId: "1:44311916194:web:8a558de19bf7119e91f1fb"
};

class Firebase {
  constructor() {
    app.initializeApp(config);

    this.fieldValue = app.firestore.FieldValue;

    this.auth = app.auth();
    this.db = app.firestore();
  }

  // *** Auth API ***

  doCreateUserWithEmailAndPassword = (email, password) =>
    this.auth.createUserWithEmailAndPassword(email, password);

  doSignInWithEmailAndPassword = (email, password) =>
    this.auth.signInWithEmailAndPassword(email, password);

  doSignOut = () => this.auth.signOut();

  doPasswordReset = email => this.auth.sendPasswordResetEmail(email);

  doPasswordUpdate = password =>
    this.auth.currentUser.updatePassword(password);

  // *** User API ***

  user = uid => this.db.doc(`users/${uid}`);

  users = () => this.db.collection('users');

  dokument = did => this.db.doc(`dokumente/${did}`);

  dokumente = () => this.db.collection('dokumente');

  dokument_oeffentlich = doid => this.db.doc(`dokumente_oeffentlich/${doid}`);

  dokumente_oeffentlich = () => this.db.collection('dokumente_oeffentlich');

  dokument_recherche = doid => this.db.doc(`dokumente_recherche/${doid}`);

  dokumente_recherche = () => this.db.collection('dokumente_recherche');

  dokument_entwurf = doid => this.db.doc(`dokumente_entwurf/${doid}`);

  dokumente_entwurf = () => this.db.collection('dokumente_entwurf');

  dokument_recherche = doid => this.db.doc(`dokumente_allgemein/${doid}`);

  dokumente_allgemein = () => this.db.collection('dokumente_allgemein');
}

export default Firebase;
